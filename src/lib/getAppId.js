const { filesystem } = require('gluegun')

module.exports = async (dir = process.cwd()) => {
  const xml = await filesystem.read(
    `${dir}/android/app/src/main/AndroidManifest.xml`,
    'utf8'
  )
  return xml.match(/package="(.+?)"/)[1]
}
