const { toLower } = require('ramda')
const exitCodes = require('../lib/exitCodes')
const { updateAppId } = require('../lib/manageNativeFiles')

module.exports = {
  name: 'package',
  description: 'Update the project package name',
  run: async toolbox => {
    const {
      parameters,
      filesystem,
      print: { info, error, colors, spin },
      strings: { isBlank }
    } = toolbox

    // Grab the project name
    const packageName = parameters.first

    // Check for a valida package name
    const isValidPackage = /^([A-Za-z]{1}[A-Za-z\d_]*\.)*[A-Za-z][A-Za-z\d_]*$/gim.test(
      packageName
    )

    // verify the project name is a thing
    if (isBlank(packageName)) {
      info(`lighthouse-tools package <packageName>\n`)
      error('Package name is required')
      process.exit(exitCodes.PACKAGE_NAME)
    }

    // verify the package name is valid
    if (!isValidPackage) {
      error(`Please, enter a valida package name. E.g. com.yourdomain.appname`)
      process.exit(exitCodes.PROJECT_NAME)
    }

    // verify if is in the root project folder
    if (filesystem.exists(`${filesystem.cwd()}/package.json`) !== 'file') {
      error(
        `Your're trying to run the command from ${filesystem.cwd()}.\n
        Please, run this command from the root directory of your project.`
      )
      process.exit(exitCodes.DIRECTORY_EXISTS)
    }

    // Clone boilerplate to the created project folder
    const spinner = spin(`Updating package name to ${toLower(packageName)}...`)
    await updateAppId(filesystem.cwd(), toLower(packageName))
    spinner.succeed(`${colors.success('Package name updated')}`)
  }
}
