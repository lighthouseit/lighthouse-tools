const { hasPath } = require('ramda')

function isReactNativeDirectory (dir) {
  const packageJson = require(`${dir}/package.json`)
  return hasPath(['react-native'], packageJson.dependencies)
}

module.exports = isReactNativeDirectory
