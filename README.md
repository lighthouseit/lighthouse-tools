# lighthouse-tools

![Lighthouse Utils](https://i.imgur.com/HU3SiA9.png)

O Lighthouse Tools é um Node-based CLI que possui um conjunto de ferramentas e atalhos que facilitam na construção e gerenciamento de projetos em React Native.

Lista de comandos disponíveis:

🌯 _init_ - Começe um projeto novo em React Native utilizando o boilerplate oficial da Lighthouse.<br />
💃 _package_ - Altere o nome de pacote do seu aplicativo em ambas plataformas Android e iOS.<br />
💻 _generate_ - Utilize templates predefinidos para criar arquivos para você. Ex.: Screens, Lists, Components, Reducers, Actions, etc.<br />

## Roadmap:

🚀 fastlane - Automatize o build e distribuição de qualquer projeto em React Native.

# Início rápido

![Example](https://i.imgur.com/vDXwvBx.png)

Apenas execute o CLI `lighthouse-tools` desta forma:

```
$ yarn global add lighthouse-tools
$ lighthouse-tools init awesomeapp
$ cd awesomeapp
$ yarn install
$ yarn run-android or yarn run-ios
```

Use `lighthouse-tools --help` para saber como utilizar os demais comandos.

## O que há por de baixo dos panos?

⭐️ [gluegun](https://github.com/infinitered/gluegun) for build powerful Node-based CLIs.<br />
⭐️ [ramdajs](https://github.com/ramda/ramda/) for easy manipulation of javascript objects.<br />
⭐️ [replace-in-file](https://github.com/adamreisnz/replace-in-file) for replace file contents in a fastest way.<br />

[Node.js 7.6+](https://nodejs.org) é obrigatório.

## Patrocinadores

[Lighthouse Tools](https://gitlab.com/lighthouseit/lighthouse-tools) é patrocinado pela [Lighthouse](https://lighthouseit.com.br/), um time de designers e desenvolvedores localizados em Porto Alegre/RS - Brasil. Nossas skills variam entre UI/UX, React, React Native, .NET Core e mais!
