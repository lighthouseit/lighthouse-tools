const { build } = require('gluegun')
const updateNotifier = require('update-notifier')
const pkg = require('../package.json')

// notify if update is needed
updateNotifier({
  pkg,
  updateCheckInterval: 0
}).notify()

/**
 * Create the cli and kick it off
 */
async function run (argv) {
  // create a CLI runtime
  const cli = build()
    .brand('lighthouse-tools')
    .src(__dirname)
    .plugins('./node_modules', { matching: 'lighthouse-tools-*', hidden: true })
    .help() // provides default for help, h, --help, -h
    .version() // provides default for version, v, --version, -v
    .create()

  // and run it
  const toolbox = await cli.run(argv)

  // send it back (for testing, mostly)
  return toolbox
}

module.exports = { run }
