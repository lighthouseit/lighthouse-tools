#!/usr/bin/env bash
# Creates .env and .env.production from ENV variables for use with react-native-config
ENV_WHITELIST=${ENV_WHITELIST:-"^RN*|^FASTLANE*"}
printf "Creating an .env file with the following whitelist:\n"
printf "%s\n\n" $ENV_WHITELIST
set | egrep -e $ENV_WHITELIST | egrep -v "^_" | egrep -v "WHITELIST" > .env
set | egrep -e $ENV_WHITELIST | egrep -v "^_" | egrep -v "WHITELIST" > .env.production
printf "\n.env and .env.production created with contents:\n"
cat .env
