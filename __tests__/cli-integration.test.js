const { system, filesystem } = require('gluegun')
const { resolve } = require('path')
const { version } = require('../package.json')
const getAppId = require('../src/lib/getAppId')

const src = resolve(__dirname, '..')

const cli = async cmd =>
  system.run('node ' + resolve(src, 'bin', 'lighthouse-tools') + ` ${cmd}`)

beforeAll(() => {
  // Cleanup artifacts
  filesystem.remove('foo')
})

afterAll(() => {
  // Cleanup artifacts
  filesystem.remove('foo')
})

test('outputs version', async () => {
  const output = await cli('--version')
  expect(output).toContain(version)
})

test('outputs help', async () => {
  const output = await cli('--help')
  expect(output).toContain(version)
})

test('initializing a new project', async () => {
  await cli('init foo FooBar com.foo.bar')
  const packageJson = filesystem.read('foo/package.json')
  expect(packageJson).toContain('react-native')
}, 3600000)

test('updating the package name of a existing project', async () => {
  await system.run(
    `cd foo && node ${resolve(
      src,
      'bin',
      'lighthouse-tools'
    )} package lh.coolest.foo`
  )
  const appId = await getAppId('foo')

  expect(appId).toEqual('lh.coolest.foo')
})

test('generates a screen', async () => {
  await system.run(
    `cd foo && node ${resolve(
      src,
      'bin',
      'lighthouse-tools'
    )} generate screen MyScreen`
  )
  const file = filesystem.read('foo/src/screens/MyScreen/MyScreen.js')

  expect(file).toContain('MyScreen')
})

test('generates a component', async () => {
  await system.run(
    `cd foo && node ${resolve(
      src,
      'bin',
      'lighthouse-tools'
    )} generate component MyComponent`
  )
  const file = filesystem.read('foo/src/components/MyComponent.js')

  expect(file).toContain('MyComponent')
})

test('generates a redux', async () => {
  await system.run(
    `cd foo && node ${resolve(
      src,
      'bin',
      'lighthouse-tools'
    )} generate redux MyTest`
  )
  const file = filesystem.read('foo/src/redux/myTestReducer.js')

  expect(file).toContain('initialState')
})
