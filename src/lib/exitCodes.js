/**
 * Exit codes used to off the app.
 */
module.exports = {
  /**
   * A peaceful & expected death.
   */
  OK: 0,

  /**
   * A generic and unexpected ending for our hero.
   */
  GENERIC: 1,

  /**
   * Cannot find React Native version.
   */
  REACT_NATIVE_VERSION: 2,

  /**
   * Invalid Project name.
   */
  PROJECT_NAME: 3,

  /**
   * You're trying to spork, but there's nothing to eat
   */
  SPORKABLES_NOT_FOUND: 4,

  /**
   * Invalid Plugin name.
   */
  PLUGIN_NAME: 5,

  /**
   * This directory already exists
   */
  DIRECTORY_EXISTS: 6,

  /**
   * Problem installing React Native
   */
  REACT_NATIVE_INSTALL: 7,

  /**
   * node_modules/react-native already exists.
   */
  EXISTING_REACT_NATIVE: 8,

  /**
   * One of the global dependencies are not met.
   */
  INVALID_GLOBAL_DEPENDENCY: 9,

  /**
   * Missing package name as param.
   */
  PACKAGE_NAME: 10,

  /**
   * directory is not an react native project.
   */
  NOT_REACT_NATIVE_PROJECT: 11,

  /**
   * context param is invalid.
   */
  INVALID_PARAMETER: 12
}
