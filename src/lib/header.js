/* eslint-disable */
const { print } = require('gluegun')

module.exports = function () {
  const {
    info,
    colors: { red, yellow }
  } = print

  info('-----------------------------------------------')
  info(red('#                                                                #######                             '))
  info(red('#       #  ####  #    # ##### #    #  ####  #    #  ####  ######    #     ####   ####  #       ####  '))
  info(red('#       # #    # #    #   #   #    # #    # #    # #      #         #    #    # #    # #      #      '))
  info(red('#       # #      ######   #   ###### #    # #    #  ####  #####     #    #    # #    # #       ####  '))
  info(red('#       # #  ### #    #   #   #    # #    # #    #      # #         #    #    # #    # #           # '))
  info(red('#       # #    # #    #   #   #    # #    # #    # #    # #         #    #    # #    # #      #    # '))
  info(red('####### #  ####  #    #   #   #    #  ####   ####   ####  ######    #     ####   ####  ######  ####  '))
  info('-----------------------------------------------')
  info('')
  info('A useful CLI for manage React Native projects at Lighthouse.')
  info(yellow('https://gitlab.com/lighthouseit/lighthouse-tools'))
  info('')
  info('-----------------------------------------------')
}
