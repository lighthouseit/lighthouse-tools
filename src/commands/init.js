const { isEmpty, match, not, toLower } = require('ramda')
const { resolve } = require('path')
const exitCodes = require('../lib/exitCodes')
const { moveIOSFiles, updateNativeFiles } = require('../lib/manageNativeFiles')
const header = require('../lib/header')
const {
  BOILERPLATE_REPO_URL,
  BOILERPLATE_DISPLAY_NAME,
  BOILERPLATE_APP_ID,
  BOILERPLATE_SLUG
} = require('../lib/constants')

module.exports = {
  name: 'init',
  description:
    'Initialize a new React Native project using the official Lighthouse Boilerplate',
  run: async toolbox => {
    const {
      parameters,
      system,
      filesystem,
      prompt,
      print: { info, error, colors, spin },
      strings: { isBlank, upperFirst, camelCase }
    } = toolbox

    // Grab the project name
    const projectName = parameters.first

    // Check for kebabs
    const isKebabCase = not(isEmpty(match(/.-/g, projectName || '')))

    // camelCase the project name for user example
    const projectNameCamel = upperFirst(camelCase(projectName))

    // Check for numbers-only names
    const isNumericOnly = /^\d+$/.test(projectName)

    // Check for alphanumeric name, beginning with a letter
    const isValidName = /^[a-z_][a-z0-9_]+$/i.test(projectName)

    // verify the project name is a thing
    if (isBlank(projectName)) {
      info(`lighthouse-tools init <projectName>\n`)
      error('Project name is required')
      process.exit(exitCodes.PROJECT_NAME)
    }

    // Guard against `ignite new ignite`
    if (toLower(projectName) === 'lighthouse') {
      error(
        `Hey...that's my name! Please name your project something other than '${projectName}'.`
      )
      process.exit(exitCodes.PROJECT_NAME)
    }

    // verify the project name isn't kebab cased
    if (isKebabCase) {
      error(
        `Please use camel case for your project name. Ex: ${projectNameCamel}`
      )
      process.exit(exitCodes.PROJECT_NAME)
    }

    // verify the project name isn't just numbers
    if (isNumericOnly) {
      error(
        `Please use at least one non-numeric character for your project name.`
      )
      process.exit(exitCodes.PROJECT_NAME)
    }

    // verify the project name is valid
    if (!isValidName) {
      error(
        `The project name can only contain alphanumeric characters and underscore, but must not begin with a number.`
      )
      process.exit(exitCodes.PROJECT_NAME)
    }

    // verify the directory doesn't exist already
    if (filesystem.exists(projectName) === 'dir') {
      error(`Directory ${projectName} already exists.`)
      const askOverwrite = async () => {
        return prompt.confirm('Do you want to overwrite this directory?')
      }

      if (parameters.options.overwrite || (await askOverwrite())) {
        info(`Overwriting ${projectName}...`)
        filesystem.remove(projectName)
      } else {
        process.exit(exitCodes.DIRECTORY_EXISTS)
      }
    }

    // Render Header
    header()

    const questions = [
      {
        type: 'input',
        name: 'projectDisplayName',
        message: 'Enter the project display name',
        default: BOILERPLATE_DISPLAY_NAME
      },
      {
        type: 'input',
        name: 'projectAppId',
        message: 'Enter the project app id',
        default: BOILERPLATE_APP_ID
      }
    ]

    // only for testing
    let projectDisplayName = parameters.second
    let projectAppId = parameters.third

    if (process.env.NODE_ENV !== 'test') {
      ;({ projectDisplayName, projectAppId } = await toolbox.prompt.ask(
        questions
      ))
    }

    // Shell scripts
    const dest = resolve(filesystem.cwd(), projectName)

    // Clone boilerplate to the created project folder
    const spinner = spin(
      colors.cyan(
        `Using the ${colors.yellow('Lighthouse')} boilerplate (Burnout)`
      )
    )
    await system.run(`mkdir ${dest}`)
    await system.run(`cd ${dest} && git clone ${BOILERPLATE_REPO_URL} .`)
    spinner.succeed(
      colors.cyan(`Added ${colors.yellow('Burnout')} boilerplate`)
    )

    // Updates native files
    spinner.start(colors.cyan('Updating project native files'))
    if (projectName !== BOILERPLATE_SLUG) {
      await moveIOSFiles(dest, projectName)
      await updateNativeFiles(
        dest,
        projectName,
        projectDisplayName,
        projectAppId
      )
    }
    spinner.succeed(colors.cyan('Native files updated'))

    // Initializing git
    spinner.start(colors.cyan('Initializing git'))
    await filesystem.remove(resolve(dest, '.git'))
    await system.run(`cd ${dest} && git init`)
    spinner.succeed(colors.cyan('Configured git'))

    // Success info
    info('')
    info(colors.yellow('Thanks yo use the Lighthouse Tools'))
    info('')
    info(
      colors.red(
        `Before run the project you need do some additional setups, read the instructions:\nhttps://gitlab.com/lighthouseit/burnout/blob/master/SETUP.md`
      )
    )
    info('')
    info('Installing dependecies:')
    info(colors.cyan(`  cd ${dest}`))
    info(colors.cyan(`  yarn install`))
    info('')
    info('Running on Android:')
    info(colors.cyan(`  yarn run-android`))
    info('')
    info('Running on iOS:')
    info(colors.cyan(`  cd ios && pod install`))
    info(colors.cyan(`  yarn run-ios`))
    info('')
    info('')
    info(colors.yellow(`🚀  1, 2, 3... Ready!`))
  }
}
