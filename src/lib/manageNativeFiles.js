const replace = require('replace-in-file')
const { resolve } = require('path')
const { filesystem, system } = require('gluegun')
const { findIndex } = require('ramda')
const getAppId = require('../lib/getAppId')
const {
  BOILERPLATE_SLUG,
  BOILERPLATE_DISPLAY_NAME
} = require('../lib/constants')

const updateAppId = async (dir, newAppId) => {
  const oldAppId = await getAppId(dir)
  const iosDirList = await filesystem.list(`${dir}/ios`)
  const xcodeprojDirIndex = findIndex(x => /.+(xcodeproj)/.test(x), iosDirList)

  return replace({
    files: [
      `${dir}/ios/${iosDirList[xcodeprojDirIndex]}/project.pbxproj`,
      `${dir}/android/app/src/main/java/lh/app/*.java`,
      `${dir}/android/app/BUCK`,
      `${dir}/android/app/build.gradle`,
      `${dir}/android/app/src/main/AndroidManifest.xml`,
      `${dir}/fastlane/Appfile`
    ],
    from: new RegExp(oldAppId, 'g'),
    to: newAppId
  })
}

const updateNativeFiles = async (dir, name, displayName, appId) => {
  const xcodeprojPath = `${dir}/ios/${name}.xcodeproj`

  // Update package name
  if (appId) {
    await updateAppId(dir, appId)
  }

  // Update app slug
  await replace({
    files: [
      `${xcodeprojPath}/project.pbxproj`,
      `${xcodeprojPath}/xcshareddata/xcschemes/${name}.xcscheme`,
      `${xcodeprojPath}/xcshareddata/xcschemes/${name}-tvOS.xcscheme`,
      `${dir}/ios/${name}Tests/${name}Tests.m`,
      `${dir}/ios/${name}/AppDelegate.m`,
      `${dir}/ios/Podfile`,
      `${dir}/app.json`,
      `${dir}/android/app/src/main/res/values/strings.xml`
    ],
    from: new RegExp(BOILERPLATE_SLUG, 'g'),
    to: name
  })
  await replace({
    files: [`${dir}/android/app/src/main/java/lh/app/MainActivity.java`],
    from: new RegExp(`"${BOILERPLATE_SLUG}"`, 'g'),
    to: `"${name}"`
  })

  // Update display name
  if (displayName) {
    await replace({
      files: [
        `${xcodeprojPath}/project.pbxproj`,
        `${dir}/app.json`,
        `${dir}/android/app/src/main/res/values/strings.xml`,
        `${dir}/ios/${name}/Info.plist`
      ],
      from: new RegExp(BOILERPLATE_DISPLAY_NAME, 'g'),
      to: displayName
    })
  }
}

const moveIOSFiles = async (dir, name) => {
  function iosDir () {
    return resolve(dir, 'ios', ...arguments)
  }

  await system.run(`mv ${iosDir(BOILERPLATE_SLUG)} ${iosDir(name)}`)
  await system.run(
    `mv ${iosDir(`${BOILERPLATE_SLUG}Tests`)} ${iosDir(name + 'Tests')}`
  )
  await system.run(
    `mv ${iosDir(`${BOILERPLATE_SLUG}-tvOS`)} ${iosDir(name + '-tvOS')}`
  )
  await system.run(
    `mv ${iosDir(`${BOILERPLATE_SLUG}-tvOSTests`)} ${iosDir(
      name + '-tvOSTests'
    )}`
  )
  await system.run(
    `mv ${iosDir(`${BOILERPLATE_SLUG}.xcodeproj`)} ${iosDir(
      name + '.xcodeproj'
    )}`
  )
  await system.run(
    `mv ${iosDir(
      `${name}.xcodeproj`,
      'xcshareddata',
      'xcschemes',
      `${BOILERPLATE_SLUG}.xcscheme`
    )} ${iosDir(
      `${name}.xcodeproj`,
      'xcshareddata',
      'xcschemes',
      name + '.xcscheme'
    )}`
  )
  await system.run(
    `mv ${iosDir(
      `${name}.xcodeproj`,
      'xcshareddata',
      'xcschemes',
      `${BOILERPLATE_SLUG}-tvOS.xcscheme`
    )} ${iosDir(
      `${name}.xcodeproj`,
      'xcshareddata',
      'xcschemes',
      name + '-tvOS.xcscheme'
    )}`
  )
  await system.run(
    `mv ${iosDir(name + 'Tests', `${BOILERPLATE_SLUG}Tests.m`)} ${iosDir(
      name + 'Tests',
      name + 'Tests.m'
    )}`
  )
  await system.run(
    `mv ${iosDir(name, `${BOILERPLATE_SLUG}.entitlements`)} ${iosDir(
      name,
      `${name}.entitlements`
    )}`
  )
}

module.exports = {
  updateAppId,
  updateNativeFiles,
  moveIOSFiles
}
