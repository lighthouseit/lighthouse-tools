#!/usr/bin/env bash
sh ./scripts/create-env-files.sh # Generates .env and .env.production files
bundle exec fastlane create_app_icons
bundle exec fastlane update_platforms_info
