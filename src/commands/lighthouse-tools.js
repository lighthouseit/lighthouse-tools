module.exports = {
  name: 'lighthouse-tools',
  run: async toolbox => {
    const commandInfo = toolbox.meta.commandInfo()
    toolbox.print.table(commandInfo)
  }
}
