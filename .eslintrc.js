module.exports = {
  extends: 'standard',
  plugins: ['jest'],
  env: {
    jest: true,
    mocha: true
  },
  globals: {
    by: true,
    expect: true,
    device: true,
    element: true,
    fetch: false,
    navigator: false
  }
}
