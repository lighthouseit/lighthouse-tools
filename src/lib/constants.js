module.exports = {
  BOILERPLATE_REPO_URL: 'https://gitlab.com/lighthouseit/burnout',
  BOILERPLATE_SLUG: 'burnout',
  BOILERPLATE_DISPLAY_NAME: 'Burnout',
  BOILERPLATE_APP_ID: 'br.com.lighthouseit.burnout',
  AVAILABLE_GENERATORS: ['screen', 'component', 'redux']
}
