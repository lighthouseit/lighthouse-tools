const { includes } = require('ramda')
const isReactNativeDirectory = require('../lib/isReactNativeDirectory')
const exitCodes = require('../lib/exitCodes')
const { AVAILABLE_GENERATORS } = require('../lib/constants')

module.exports = {
  name: 'generate',
  alias: ['g'],
  description: 'Generates some files',
  run: async toolbox => {
    const {
      template,
      runtime,
      parameters,
      patching,
      print: { colors, info, error },
      strings: { pascalCase, lowerCase, camelCase, upperCase, isBlank }
    } = toolbox

    // ensure we're in a supported directory
    if (!isReactNativeDirectory(process.cwd())) {
      error(
        'The `generate` command must be run in an react native project directory.'
      )
      process.exit(exitCodes.INVALID_PARAMETER)
    }

    if (isBlank(parameters.first)) {
      info(`${runtime.brand} generate <generator-type> <name>\n`)
      info('Generator type not specified.')
      process.exit(exitCodes.INVALID_PARAMETER)
    }

    if (!includes(parameters.first, AVAILABLE_GENERATORS)) {
      info('The generator specified is not valid.\n')
      info(`The available generators are ${AVAILABLE_GENERATORS.join(', ')}.`)
      process.exit(exitCodes.INVALID_PARAMETER)
    }

    const name = parameters.second

    if (isBlank(name)) {
      info(`${runtime.brand} generate <generator-type> <name>\n`)
      info('File name not specified.')
      process.exit(exitCodes.INVALID_PARAMETER)
    }

    const pascalName = pascalCase(name)
    const camelName = camelCase(name)
    const upperName = upperCase(name)

    switch (lowerCase(parameters.first)) {
      case 'screen':
        await template.generate({
          template: 'generators/screen.js.ejs',
          target: `src/screens/${pascalName}/${pascalName}.js`,
          props: { pascalName, camelName, upperName }
        })
        await template.generate({
          template: 'generators/screen-index.js.ejs',
          target: `src/screens/${pascalName}/index.js`,
          props: { pascalName, camelName, upperName }
        })
        info(colors.cyan(`Screen ${pascalName} generated fine.`))
        break
      case 'component':
        await template.generate({
          template: 'generators/component.js.ejs',
          target: `src/components/${pascalName}.js`,
          props: { pascalName, camelName, upperName }
        })
        info(colors.cyan(`Component ${pascalName} generated fine.`))
        break
      case 'redux':
        await template.generate({
          template: 'generators/action.js.ejs',
          target: `src/redux/actions/${camelName}Actions.js`,
          props: { pascalName, camelName, upperName }
        })
        await template.generate({
          template: 'generators/reducer.js.ejs',
          target: `src/redux/${camelName}Reducer.js`,
          props: { pascalName, camelName, upperName }
        })
        await patching.patch('src/redux/index.js', {
          insert: `\nimport ${camelName}Reducer from './${camelName}Reducer';`,
          after: `import globalReducer from './globalReducer';`
        })
        await patching.patch('src/redux/index.js', {
          insert: `\n  ${camelName}: ${camelName}Reducer,`,
          after: `global: globalReducer,`
        })
        info(colors.cyan(`Redux for ${pascalName} generated fine.`))
        break
      default:
        break
    }
  }
}
